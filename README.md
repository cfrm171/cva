# cva

Credit valuation adjustment (CVA) is the market price of counterparty credit risk that has become a central part of counterparty credit risk management.  By definition, CVA is the difference between the risk-free portfolio value and the true/risky po